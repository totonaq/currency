import React from 'react';

const Header = props => {
    return (
        <div className='app-header'>
            <h1 className='app-header__text'>Cryptocurrency exchange</h1>
        </div>
    );
};

export default Header;
Header.displayName = 'Header';